#include <raylib.h>

struct Ball {
    float x, y;
    float speedX, speedY;
    float radius;

    void Draw() {
        DrawCircle(x, y, radius, WHITE);
    }
};

struct Paddle {
    float x, y;
    float speed;
    float width, height;

    Rectangle GetRect() {
        return Rectangle {x - width / 2, y - height / 2, 10, 100};
    }

    void Draw() {
        // DrawRectangle(x - width / 2, y - height / 2, width, height, WHITE);            
        // Make the x and y pos the center of the paddle instead of the corner
        DrawRectangleRec(GetRect(), WHITE);
        // Using rect to draw
    }
};

int main() {
    InitWindow(800, 600, "Pong");

    // SetTargetFPS(60);
    SetWindowState(FLAG_VSYNC_HINT);
    // Set max framerate to screen refreshrate
    
    Ball ball;
    ball.x = (float) GetScreenWidth() / 2;
    ball.y = (float) GetScreenHeight() / 2;
    ball.radius = 5;
    ball.speedX = 300;
    ball.speedY = 0;

    Paddle leftPaddle;
    leftPaddle.x = 50;
    leftPaddle.y = (float) GetScreenHeight() / 2;
    leftPaddle.width = 10;
    leftPaddle.height = 100;
    leftPaddle.speed = 500;

    Paddle rightPaddle;
    rightPaddle.x = (float) GetScreenWidth() - 50;
    rightPaddle.y = (float) GetScreenHeight() / 2;
    rightPaddle.width = 10;
    rightPaddle.height = 100;
    rightPaddle.speed = 500;

    const char * winnerText = nullptr;

    while (!WindowShouldClose()) {

        // Adding speed to the ball
        ball.x += ball.speedX * GetFrameTime();
        ball.y += ball.speedY * GetFrameTime();
        // Multiply by windows frames to keep speed steady
        // Higher framerate will make the ball faster

        // Condition to check if ball is hitting the screen
        if (ball.y < 0) {
            ball.y = 0;
            ball.speedY *= -1;
        }

        if (ball.y > GetScreenHeight()) {
            ball.y = GetScreenHeight();
            // Prevent bug where ball gets stuck on edge of screen
            ball.speedY *= -1;
            // Reverses speed
        }

        // Condition to move the paddles
        if (IsKeyDown(KEY_W)) { // UP
            if (leftPaddle.y - leftPaddle.height / 2 <= 0) {
                leftPaddle.y = 0 + leftPaddle.height / 2;
            }
            // Condition to keep paddles from leaving screen
            leftPaddle.y -= leftPaddle.speed * GetFrameTime();
        }
        if (IsKeyDown(KEY_S)) { // DOWN
            if (leftPaddle.y + leftPaddle.height / 2 >= GetScreenHeight()) {
                leftPaddle.y = GetScreenHeight() - leftPaddle.height / 2;
            }
            leftPaddle.y += leftPaddle.speed * GetFrameTime();
        }

        if (IsKeyDown(KEY_UP)) { // UP
            if (rightPaddle.y - rightPaddle.height / 2 <= 0) {
                rightPaddle.y = 0 + rightPaddle.height / 2;
            }
            rightPaddle.y -= rightPaddle.speed * GetFrameTime();
        }
        if (IsKeyDown(KEY_DOWN)) { // DOWN
            if (rightPaddle.y + rightPaddle.height / 2 >= GetScreenHeight()) {
                rightPaddle.y = GetScreenHeight() - rightPaddle.height / 2;
            }
            rightPaddle.y += rightPaddle.speed * GetFrameTime();
        }

        // Condition to check collision between ball and paddle
        if (CheckCollisionCircleRec(Vector2 {ball.x, ball.y}, ball.radius, leftPaddle.GetRect())) {
            if (ball.speedX < 0) {
                ball.speedX *= -1.1f;
                // 1.1 speeds up the ball a little each collision
                ball.speedY = (ball.y - leftPaddle.y) / (leftPaddle.height / 2) * ball.speedX;
                // Make it so the ball moves according to where on the paddle it hit (top or bottom)
                // Normalize the value
            }
            // Prevent bug and force ball to go opposite direction of paddle
        }

        if (CheckCollisionCircleRec(Vector2 {ball.x, ball.y}, ball.radius, rightPaddle.GetRect())) {
            if (ball.speedX > 0) {
                ball.speedX *= -1.1f;
                ball.speedY = (ball.y - rightPaddle.y) / (rightPaddle.height / 2) * ball.speedX;
                // Multiply by speed x so that when it gets faster so does y
            }
        }

        // Condition to win game
        if (ball.x < 0) {
            winnerText = "Right Player Wins!";
        }
        if (ball.x > GetScreenWidth()) {
            winnerText = "Left Player Wins!";
        }

        BeginDrawing();
            ClearBackground(BLACK);

            ball.Draw();
            leftPaddle.Draw();
            rightPaddle.Draw();

            if (winnerText) { // Is not nullptr
                int textWidth = MeasureText(winnerText, 60);
                DrawText(winnerText, GetScreenWidth() / 2 - textWidth / 2, GetScreenHeight() / 2 - 30, 60, YELLOW);
            }

            if (winnerText && IsKeyPressed(KEY_SPACE)) {
                ball.x = (float) GetScreenWidth() / 2;
                ball.x = (float) GetScreenHeight() / 2;
                ball.speedX = 300;
                ball.speedY = 0;
                winnerText = nullptr;
            }
            // Reset if game over

            DrawFPS(10, 10);
        EndDrawing(); 
    }

    CloseWindow();

    return 0;
}